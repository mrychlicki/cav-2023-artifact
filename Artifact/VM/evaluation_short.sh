#!/bin/bash
echo "Start running 'make'"
cd /home/cav23/Desktop/fairsyn/build
make
cd /home/cav23/Desktop/mascotsds/build
make

echo ""
echo "Start generating logs for table 1"
cd /home/cav23/Desktop/fairsyn/run_bash
./cav23_short.sh
cp general_0.txt /home/cav23/Desktop/table1_logs_short.txt
cd /home/cav23/Desktop

echo ""
echo "Start generating logs for table 2"
cd /home/cav23/Desktop/mascotsds/examples/rabin_examples/bistable_switch_cfg_format
./cav23_short.sh
cp switch.log /home/cav23/Desktop/table2_logs_short.txt
cd /home/cav23/Desktop


echo ""
echo "Start generating logs for table 2 stochastic synthesis"
cd /home/cav23/Desktop/dutreix_stochasticsynthesis
./cav23_short.sh
cp logfile.txt /home/cav23/Desktop/table2_logs_short_stochastic_synthesis.txt
cd /home/cav23/Desktop

