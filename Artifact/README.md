This is the artifact with submission number 491,
titled "A Flexible Toolchain for Symbolic Rabin Games under Fair and Stochastic Uncertainties".
The artifact will remain publicly available in the provided link, 
and the link has been included in our tools repositories:
- https://gitlab.mpi-sws.org/mrychlicki/genie
- https://gitlab.mpi-sws.org/kmallik/mascotsds
- https://gitlab.mpi-sws.org/kmallik/fairsyn

We apply for all three badges namely Functional, Reusable, and Available.

# How to mount the virtual machine image in virtualbox
- Download and unzip file.
- Right click `cav23.ova` and choose "Open with VirtualBox"
- VirtualBox will pop up with extra window, click "Import"
- When the loading is done, select "cav23" from the available machines in the virtualbox
- To log in to the machine, enter the password `cav23`

# How to generate results from paper
The results depend on the machine on which they are run.
All the experiments were run on a computer equipped with
Intel Xeon E7-8857 v2 48 core processor and 1.5 TB RAM
To generate all the results, run the script:
``./evaluation.sh``.

We have also created a script ``./evaluation_short.sh`` that could be easily run on laptops.
This script generates results for cases (1,1), (2,1) and (3,1) from Table 1
and the first two rows from Table 2 with both specifications, for both sylvan and cudd.
It takes around 1h to run `./evaluation_short.sh` on a laptop equipped with
Intel i5-9300H, NVIDIA GeForce GTX 1650 + Intel UHD Graphics 630 and 8 GB RAM.

Both scripts will create logs inside the folder `/home/cav23/Desktop/`.

# File structure in the virutal machine
All of the following folders and files are located in  `/home/cav23/Desktop/`.

- `./evaluation.sh` - Script to generate all results from papers.
- `./evaluation_short.sh` - Script to generate a subset of results from papers.
The running time on aa laptop is 1 hour.
- `./table1_orginal_logs.txt`, `./table2_orginal_logs.txt`, `./table2_orginal_logs_stochastic_synthesis.txt` - Files containing the logs that were used in the paper.
- `./genie/` - Our library for generating experiments on Binary Decision Diagram.
  - `./genie/docs/html/index.html` - Genie's documentation.
- `./fairsyn/` - Parallel reactive synthesis for omega-regular specifications under the transition fairness condition.
  - `./fairsyn/src/general_queue.cc` - The main file which is run to calculate the results from Table 1.
  - `./fairsyn/run_bash/`- Folder that contains experiments from Table 1 and scripts: `cav23.sh` and `cav_short.sh`.
  - `./fairsyn/docs/html/index.html` - Fairsyn's documentation.
- `./masctosds/` - Controller synthesis for stochastic dynamical system using finite abstraction
  - `./masctosds/src/masctot-synt.cc` - The main file which is run to calculate the results from Table 2.
  - `./examples/rabin_examples/bistable_switch_cfg_format` - Folder that contains experiments from Table 2 and scripts: `cav23.sh` and `cav_short.sh`.
  - `./examples/rabin_examples/bistable_switch_cfg_format/switch_abs_${x}_spec_${y}.cfg` - Experiment with `${x}` equal 
to 3,4,5,6 to corresponds to experiment with upperbound 20%-30%, 10%-20%, 5%-10%, 0%-5% respectively.
Variable `${y}` corresponds to the specification number.
  - `./masctosds/docs/html/index.html` - Mascot's documentation.
- `./dutreix_stochasticsynthesis/` - Modified repository of [StochasticSynthesis](https://github.com/gtfactslab/StochasticSynthesis). Used to compare results in Table 2. This folder contains experiments from Table 2 and scripts: `cav23.sh` and `cav_short.sh`.
- `./submodules/` - Folder contains all the projects needed to run Genie.
  - `./submodules/cudd` - [Cudd](https://github.com/ivmai/cudd)
  - `./submodules/sylvan` - [Sylvan](https://github.com/trolando/sylvan)
  - `./submodules/cpphoafparser` - [CppHoafParser](https://automata.tools/hoa/cpphoafparser/index.html)
- `./libraries/` - Folder where all projects from `./submodules/` are installed.
- `./owl/` - The [Owl](https://github.com/owl-toolkit/owl/releases/tag/release-21.0) project folder that MascotSDS needs.

# Licences

Repositories: `genie`, `mascotsds` and `fairsyn` have licences in their folders.
We have not added a licence to `dutreix_stochasticsynthesis` because it contains a small modification of
the [StochasticSynthesis](https://github.com/gtfactslab/StochasticSynthesis) repository.

# Usage beyond tha paper

The user interface of our tool is already explained in sufficient detail in the tool paper, which will make it easy to use the tool for running other examples as well. Moreover, the tools are well-documented using doxygen, and all the functionalities are explained in detail. Thus developers can also easily extend and build upon our tool."

# Documentation

All documentation can be found in `<repository_folder>/docs/html/index.html`,
which was generated from `<repository>/docs/Doxyfile.in` using instruction from `<repository>/README.md`.

# Installation

All the necessary files are already installed in the virtual machine.
However, if you wish to install our repositories on your computer,
you must follow the instructions in the `<repository>/README.md`.
We have created a user-friendly installation.
If the user has the basic libraries, which are listed in `genie/README.md`,
just run `genie/install.sh` and then specify path when starting cmake.
